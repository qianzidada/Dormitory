﻿namespace 宿舍管理系统
{
    partial class 迁出登记
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(迁出登记));
            this.dateTime = new System.Windows.Forms.DateTimePicker();
            this.txtshiyou = new System.Windows.Forms.TextBox();
            this.lblshiyou = new System.Windows.Forms.Label();
            this.lbltime = new System.Windows.Forms.Label();
            this.txtbroomnumber = new System.Windows.Forms.TextBox();
            this.lblbroomnumber = new System.Windows.Forms.Label();
            this.txtname = new System.Windows.Forms.TextBox();
            this.lblname = new System.Windows.Forms.Label();
            this.lblsex = new System.Windows.Forms.Label();
            this.btnnexit = new System.Windows.Forms.Button();
            this.btndengji = new System.Windows.Forms.Button();
            this.txtID = new System.Windows.Forms.TextBox();
            this.lblID = new System.Windows.Forms.Label();
            this.txtsex = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // dateTime
            // 
            this.dateTime.Location = new System.Drawing.Point(102, 147);
            this.dateTime.Name = "dateTime";
            this.dateTime.Size = new System.Drawing.Size(150, 21);
            this.dateTime.TabIndex = 28;
            // 
            // txtshiyou
            // 
            this.txtshiyou.Location = new System.Drawing.Point(102, 196);
            this.txtshiyou.Multiline = true;
            this.txtshiyou.Name = "txtshiyou";
            this.txtshiyou.Size = new System.Drawing.Size(328, 57);
            this.txtshiyou.TabIndex = 27;
            this.txtshiyou.TextChanged += new System.EventHandler(this.txtshiyou_TextChanged);
            // 
            // lblshiyou
            // 
            this.lblshiyou.AutoSize = true;
            this.lblshiyou.Location = new System.Drawing.Point(27, 199);
            this.lblshiyou.Name = "lblshiyou";
            this.lblshiyou.Size = new System.Drawing.Size(53, 12);
            this.lblshiyou.TabIndex = 26;
            this.lblshiyou.Text = "迁出事由";
            // 
            // lbltime
            // 
            this.lbltime.AutoSize = true;
            this.lbltime.Location = new System.Drawing.Point(27, 151);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(53, 12);
            this.lbltime.TabIndex = 25;
            this.lbltime.Text = "迁出时间";
            // 
            // txtbroomnumber
            // 
            this.txtbroomnumber.Location = new System.Drawing.Point(313, 43);
            this.txtbroomnumber.Name = "txtbroomnumber";
            this.txtbroomnumber.Size = new System.Drawing.Size(100, 21);
            this.txtbroomnumber.TabIndex = 24;
            // 
            // lblbroomnumber
            // 
            this.lblbroomnumber.AutoSize = true;
            this.lblbroomnumber.Location = new System.Drawing.Point(218, 46);
            this.lblbroomnumber.Name = "lblbroomnumber";
            this.lblbroomnumber.Size = new System.Drawing.Size(77, 12);
            this.lblbroomnumber.TabIndex = 23;
            this.lblbroomnumber.Text = "牵出人宿舍号";
            // 
            // txtname
            // 
            this.txtname.Location = new System.Drawing.Point(102, 87);
            this.txtname.Name = "txtname";
            this.txtname.Size = new System.Drawing.Size(100, 21);
            this.txtname.TabIndex = 19;
            // 
            // lblname
            // 
            this.lblname.AutoSize = true;
            this.lblname.Location = new System.Drawing.Point(12, 87);
            this.lblname.Name = "lblname";
            this.lblname.Size = new System.Drawing.Size(65, 12);
            this.lblname.TabIndex = 18;
            this.lblname.Text = "登记人姓名";
            // 
            // lblsex
            // 
            this.lblsex.AutoSize = true;
            this.lblsex.Location = new System.Drawing.Point(230, 90);
            this.lblsex.Name = "lblsex";
            this.lblsex.Size = new System.Drawing.Size(65, 12);
            this.lblsex.TabIndex = 17;
            this.lblsex.Text = "登记人性别";
            // 
            // btnnexit
            // 
            this.btnnexit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnnexit.Location = new System.Drawing.Point(449, 170);
            this.btnnexit.Name = "btnnexit";
            this.btnnexit.Size = new System.Drawing.Size(75, 23);
            this.btnnexit.TabIndex = 16;
            this.btnnexit.Text = "取消";
            this.btnnexit.UseVisualStyleBackColor = true;
            this.btnnexit.Click += new System.EventHandler(this.btnnexit_Click);
            // 
            // btndengji
            // 
            this.btndengji.Location = new System.Drawing.Point(449, 117);
            this.btndengji.Name = "btndengji";
            this.btndengji.Size = new System.Drawing.Size(75, 23);
            this.btndengji.TabIndex = 15;
            this.btndengji.Text = "登记";
            this.btndengji.UseVisualStyleBackColor = true;
            this.btndengji.Click += new System.EventHandler(this.btndengji_Click);
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(102, 42);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(100, 21);
            this.txtID.TabIndex = 29;
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.Location = new System.Drawing.Point(3, 46);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(77, 12);
            this.lblID.TabIndex = 30;
            this.lblID.Text = "牵出学生学号";
            // 
            // txtsex
            // 
            this.txtsex.FormattingEnabled = true;
            this.txtsex.Location = new System.Drawing.Point(313, 84);
            this.txtsex.Name = "txtsex";
            this.txtsex.Size = new System.Drawing.Size(100, 20);
            this.txtsex.TabIndex = 31;
            // 
            // 迁出登记
            // 
            this.AcceptButton = this.btndengji;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.CancelButton = this.btnnexit;
            this.ClientSize = new System.Drawing.Size(582, 295);
            this.Controls.Add(this.txtsex);
            this.Controls.Add(this.lblID);
            this.Controls.Add(this.txtID);
            this.Controls.Add(this.dateTime);
            this.Controls.Add(this.txtshiyou);
            this.Controls.Add(this.lblshiyou);
            this.Controls.Add(this.lbltime);
            this.Controls.Add(this.txtbroomnumber);
            this.Controls.Add(this.lblbroomnumber);
            this.Controls.Add(this.txtname);
            this.Controls.Add(this.lblname);
            this.Controls.Add(this.lblsex);
            this.Controls.Add(this.btnnexit);
            this.Controls.Add(this.btndengji);
            this.ForeColor = System.Drawing.SystemColors.Desktop;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "迁出登记";
            this.Text = "迁出登记";
            this.Load += new System.EventHandler(this.迁出登记_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateTime;
        private System.Windows.Forms.TextBox txtshiyou;
        private System.Windows.Forms.Label lblshiyou;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.TextBox txtbroomnumber;
        private System.Windows.Forms.Label lblbroomnumber;
        private System.Windows.Forms.TextBox txtname;
        private System.Windows.Forms.Label lblname;
        private System.Windows.Forms.Label lblsex;
        private System.Windows.Forms.Button btnnexit;
        private System.Windows.Forms.Button btndengji;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.ComboBox txtsex;
    }
}