USE [master]
GO
/****** Object:  Database [DdormitoryDB]    Script Date: 2020/6/17 19:31:25 ******/
CREATE DATABASE [DdormitoryDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'sufei_Data', FILENAME = N'D:\Data\DB\sufei_Data.MDF' , SIZE = 2688KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10%)
 LOG ON 
( NAME = N'sufei_Log', FILENAME = N'D:\Data\DB\sufei_Log.LDF' , SIZE = 3136KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10%)
GO
ALTER DATABASE [DdormitoryDB] SET COMPATIBILITY_LEVEL = 90
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DdormitoryDB].[dbo].[sp_fulltext_database] @action = 'disable'
end
GO
ALTER DATABASE [DdormitoryDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DdormitoryDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DdormitoryDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DdormitoryDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DdormitoryDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [DdormitoryDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [DdormitoryDB] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [DdormitoryDB] SET AUTO_SHRINK ON 
GO
ALTER DATABASE [DdormitoryDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DdormitoryDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DdormitoryDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DdormitoryDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DdormitoryDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DdormitoryDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DdormitoryDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DdormitoryDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [DdormitoryDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DdormitoryDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DdormitoryDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DdormitoryDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DdormitoryDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DdormitoryDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DdormitoryDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DdormitoryDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [DdormitoryDB] SET  MULTI_USER 
GO
ALTER DATABASE [DdormitoryDB] SET PAGE_VERIFY TORN_PAGE_DETECTION  
GO
ALTER DATABASE [DdormitoryDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DdormitoryDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DdormitoryDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [DdormitoryDB]
GO
/****** Object:  User [sufei]    Script Date: 2020/6/17 19:31:25 ******/
CREATE USER [sufei] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[sufei]
GO
/****** Object:  DatabaseRole [fe]    Script Date: 2020/6/17 19:31:25 ******/
CREATE ROLE [fe]
GO
ALTER ROLE [fe] ADD MEMBER [sufei]
GO
/****** Object:  Schema [fe]    Script Date: 2020/6/17 19:31:25 ******/
CREATE SCHEMA [fe]
GO
/****** Object:  Schema [sufei]    Script Date: 2020/6/17 19:31:25 ******/
CREATE SCHEMA [sufei]
GO
/****** Object:  Table [dbo].[centtable]    Script Date: 2020/6/17 19:31:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[centtable](
	[扣分序号] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[楼层号] [varchar](20) NOT NULL,
	[宿舍号] [varchar](20) NOT NULL,
	[扣分值] [int] NOT NULL,
	[检查人] [varchar](30) NOT NULL,
	[扣分时间] [varchar](30) NOT NULL,
	[扣分项] [varchar](100) NOT NULL,
 CONSTRAINT [PK_centtable] PRIMARY KEY CLUSTERED 
(
	[扣分序号] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[levelogion]    Script Date: 2020/6/17 19:31:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[levelogion](
	[学号] [varchar](50) NOT NULL,
	[姓名] [varchar](15) NOT NULL,
	[性别] [varchar](4) NOT NULL,
	[宿舍号] [varchar](20) NOT NULL,
	[迁出时间] [varchar](50) NOT NULL,
	[迁出事由] [varchar](200) NOT NULL,
 CONSTRAINT [PK_levelogion] PRIMARY KEY CLUSTERED 
(
	[学号] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[onduty]    Script Date: 2020/6/17 19:31:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[onduty](
	[值班序号] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[所在楼层] [varchar](20) NOT NULL,
	[值班时间] [varchar](20) NOT NULL,
	[值班人] [varchar](20) NOT NULL,
	[备注] [text] NOT NULL,
 CONSTRAINT [PK_onduty] PRIMARY KEY CLUSTERED 
(
	[值班序号] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[roomdorm]    Script Date: 2020/6/17 19:31:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[roomdorm](
	[楼层号] [varchar](50) NOT NULL,
	[宿舍号] [varchar](20) NOT NULL,
	[楼层类别] [varchar](30) NOT NULL,
	[楼层类型] [varchar](30) NOT NULL,
	[宿舍长] [varchar](30) NULL,
	[已住人数] [int] NOT NULL,
	[空位] [int] NOT NULL,
	[是否住满] [varchar](4) NULL,
 CONSTRAINT [PK_roomdorm] PRIMARY KEY CLUSTERED 
(
	[宿舍号] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Schoolin]    Script Date: 2020/6/17 19:31:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Schoolin](
	[来访序号] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[姓名] [varchar](30) NOT NULL,
	[性别] [varchar](15) NOT NULL,
	[被访学生姓名] [varchar](30) NOT NULL,
	[被访学生宿舍号] [varchar](20) NOT NULL,
	[来访时间] [varchar](50) NOT NULL,
	[在校职位] [varchar](50) NOT NULL,
	[来访事由] [varchar](50) NULL,
 CONSTRAINT [PK_Schoolin] PRIMARY KEY CLUSTERED 
(
	[来访序号] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SchoolOutlogion]    Script Date: 2020/6/17 19:31:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SchoolOutlogion](
	[来访序号] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[姓名] [varchar](15) NOT NULL,
	[性别] [varchar](4) NOT NULL,
	[被访学生姓名] [varchar](15) NOT NULL,
	[被访学生宿舍号] [varchar](20) NOT NULL,
	[来访时间] [varchar](50) NOT NULL,
	[来访事由] [varchar](200) NOT NULL,
 CONSTRAINT [PK_SchoolOutlogion] PRIMARY KEY CLUSTERED 
(
	[来访序号] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[student]    Script Date: 2020/6/17 19:31:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[student](
	[学号] [varchar](50) NOT NULL,
	[楼层号] [varchar](50) NOT NULL,
	[宿舍号] [varchar](20) NOT NULL,
	[姓名] [varchar](50) NOT NULL,
	[性别] [varchar](50) NOT NULL,
	[年龄] [int] NOT NULL,
	[系别] [varchar](50) NOT NULL,
	[班级] [varchar](50) NOT NULL,
	[年级] [varchar](50) NULL,
	[辅导员] [varchar](50) NOT NULL,
	[年制] [int] NOT NULL,
	[联系电话] [varchar](50) NULL,
	[入宿时间] [varchar](50) NULL,
	[家庭详细信息] [varchar](100) NULL,
 CONSTRAINT [PK_student] PRIMARY KEY CLUSTERED 
(
	[学号] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserInfo]    Script Date: 2020/6/17 19:31:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserInfo](
	[UserName] [varchar](50) NOT NULL,
	[Passord] [varchar](50) NOT NULL,
 CONSTRAINT [PK_user] PRIMARY KEY CLUSTERED 
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[UserInfo] ([UserName], [Passord]) VALUES (N'管理员', N'123456')
INSERT [dbo].[UserInfo] ([UserName], [Passord]) VALUES (N'学生学号', N'123456')
ALTER TABLE [dbo].[centtable]  WITH CHECK ADD  CONSTRAINT [FK_centtable_roomdorm] FOREIGN KEY([宿舍号])
REFERENCES [dbo].[roomdorm] ([宿舍号])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[centtable] CHECK CONSTRAINT [FK_centtable_roomdorm]
GO
ALTER TABLE [dbo].[levelogion]  WITH CHECK ADD  CONSTRAINT [FK_levelogion_roomdorm] FOREIGN KEY([宿舍号])
REFERENCES [dbo].[roomdorm] ([宿舍号])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[levelogion] CHECK CONSTRAINT [FK_levelogion_roomdorm]
GO
ALTER TABLE [dbo].[student]  WITH CHECK ADD  CONSTRAINT [FK_student_roomdorm] FOREIGN KEY([宿舍号])
REFERENCES [dbo].[roomdorm] ([宿舍号])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[student] CHECK CONSTRAINT [FK_student_roomdorm]
GO
USE [master]
GO
ALTER DATABASE [DdormitoryDB] SET  READ_WRITE 
GO
