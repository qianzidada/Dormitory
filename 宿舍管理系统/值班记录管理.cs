﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace 宿舍管理系统
{
    public partial class 值班记录管理 : Form
    {
        public 值班记录管理()
        {
            InitializeComponent();
        } SqlConnection objSqlConnection;
        SqlDataAdapter objSqlAdapter;
        sufeiDataSet8 sufeidataset1;

        private void roomfool()//执行按学号查询的方法
        {
            try
            {
                objSqlConnection.Open();//打开连接
                string mysql = "select * from onduty where 所在楼层=" + "'" + this.txtName.Text.ToString() + "'";
                objSqlAdapter = new SqlDataAdapter(mysql, objSqlConnection);
                sufeidataset1 = new sufeiDataSet8();
                objSqlAdapter.Fill(sufeidataset1, "onduty");
                dgMyGrid.DataSource = sufeidataset1.Tables["onduty"];//把得到的值给DataGrid
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
            finally
            {
                objSqlConnection.Close();
            }
        }
        private void roomtime()//执行按学号查询的方法
        {
            try
            {
                objSqlConnection.Open();//打开连接
                string mysql = "select * from onduty where 值班时间=" + "'" + this.cmbtime.Text.ToString() + "'";
                objSqlAdapter = new SqlDataAdapter(mysql, objSqlConnection);
                sufeidataset1 = new sufeiDataSet8();
                objSqlAdapter.Fill(sufeidataset1, "onduty");
                dgMyGrid.DataSource = sufeidataset1.Tables["onduty"];//把得到的值给DataGrid
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
            finally
            {
                objSqlConnection.Close();
            }
        }
        private void roompeople()//执行按学号查询的方法
        {
            try
            {
                objSqlConnection.Open();//打开连接
                string mysql = "select * from onduty where 值班人=" + "'" + this.txtName.Text.ToString() + "'";
                objSqlAdapter = new SqlDataAdapter(mysql, objSqlConnection);
                sufeidataset1 = new sufeiDataSet8();
                objSqlAdapter.Fill(sufeidataset1, "onduty");
                dgMyGrid.DataSource = sufeidataset1.Tables["onduty"];//把得到的值给DataGrid
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
            finally
            {
                objSqlConnection.Close();
            }
        }

        private void 值班记录管理_Load(object sender, EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“sufeiDataSet8.onduty”中。您可以根据需要移动或移除它。
            //this.ondutyTableAdapter.Fill(this.sufeiDataSet8.onduty);

            try
            {
                objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["宿舍管理系统.Properties.Settings.sufeiConnectionString"].ConnectionString);
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message.ToString() + "没有可有的数据库或连接不成功，请自行检测！！！");
            }
            this.listconbox.Items.Add("楼层号");
            this.listconbox.Items.Add("值班时间");
            this.listconbox.Items.Add("值班人");
        }

        private void listconbox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.listconbox.SelectedIndex.Equals(0))
                {
                    this.cmbtime.Visible = false;
                    this.txtName.Visible = true;
                    this.lblName.Text = "楼层号：";

                }

                else if (this.listconbox.SelectedIndex.Equals(1))
                {
                    this.lblName.Text = "值班时间时间：";
                    this.txtName.Visible = false;
                    this.cmbtime.Visible = true;

                }
                else if (this.listconbox.SelectedIndex.Equals(2))
                {
                    this.cmbtime.Visible = false;
                    this.txtName.Visible = true;
                    this.lblName.Text = "值班人：";

                }


            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message.ToString());
            }
        }

        private void btncaxun_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.listconbox.SelectedIndex.Equals(0))
                {
                    this.roomfool();
                }
                else if (this.listconbox.SelectedIndex.Equals(1))
                {
                    this.roomtime();
                }
                else if (this.listconbox.SelectedIndex.Equals(2))
                {
                    this.roompeople();
                }



            }
            catch (Exception)
            {
                MessageBox.Show("请选择查询的方式");
            }
        }

        private void btnfull_Click(object sender, EventArgs e)
        {
            try
            {
                objSqlConnection.Open();//打开连接
                string mysql = "select * from onduty";
                objSqlAdapter = new SqlDataAdapter(mysql, objSqlConnection);
                sufeidataset1 = new sufeiDataSet8();
                objSqlAdapter.Fill(sufeidataset1, "onduty");
                dgMyGrid.DataSource = sufeidataset1.Tables["onduty"];//把得到的值给DataGrid
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
            finally
            {
                objSqlConnection.Close();
            }
        }

        private void 新增ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            值班记录添加 onduty = new 值班记录添加();
            onduty.Show();
        }

        private void 退出ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void 删除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult dlResult = MessageBox.Show(this, "要删除当前的记录吗？", "请确认",
                   MessageBoxButtons.YesNo,
                   MessageBoxIcon.Question,
                   MessageBoxDefaultButton.Button1,
                   MessageBoxOptions.RightAlign);
                if (dlResult == DialogResult.Yes)
                {
                    if (this.sufeidataset1 != null)
                    {

                        this.dgMyGrid.Rows.RemoveAt(dgMyGrid.CurrentRow.Index);
                        this.ondutyTableAdapter.Update(this.sufeidataset1.onduty);
                        MessageBox.Show("删除成功");
                        this.ondutyTableAdapter.Fill(this.sufeidataset1.onduty);
                    }
                    else
                    {
                        MessageBox.Show("表中没有数据");
                    }

                }
            }

            catch (Exception ee)
            {
                MessageBox.Show(ee.Message.ToString());
            }

        }

        private void 更新ToolStripMenuItem_Click(object sender, EventArgs e)
        {

            try
            {
                this.ondutyTableAdapter.Update(this.sufeidataset1.onduty);
                MessageBox.Show("更新成功");
                this.ondutyTableAdapter.Fill(this.sufeidataset1.onduty);
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message.ToString() + "类型不正确！！！");
            }
        }

        private void dgMyGrid_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show("类型不正确！！！");
        }

        private void resfh_Click(object sender, EventArgs e)
        {
            try
            {
               
                this.ondutyTableAdapter.Fill(this.sufeidataset1.onduty);
            }
            catch (Exception ee)
            {
                MessageBox.Show("请先加载数据!!!!"+ee.Message.ToString());
            }

        }
    }
}