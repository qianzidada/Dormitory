﻿namespace 宿舍管理系统
{
    partial class 校内人员登记
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.dateTime = new System.Windows.Forms.DateTimePicker();
            this.txtshiyou = new System.Windows.Forms.TextBox();
            this.lblshiyou = new System.Windows.Forms.Label();
            this.lbltime = new System.Windows.Forms.Label();
            this.txtbroomnumber = new System.Windows.Forms.TextBox();
            this.lblbroomnumber = new System.Windows.Forms.Label();
            this.txtbname = new System.Windows.Forms.TextBox();
            this.lblbname = new System.Windows.Forms.Label();
            this.txtname = new System.Windows.Forms.TextBox();
            this.lblname = new System.Windows.Forms.Label();
            this.lblsex = new System.Windows.Forms.Label();
            this.btnnexit = new System.Windows.Forms.Button();
            this.btndengji = new System.Windows.Forms.Button();
            this.lblappintment = new System.Windows.Forms.Label();
            this.txtzhiwu = new System.Windows.Forms.TextBox();
            this.txtsex = new System.Windows.Forms.ComboBox();
            this.btnstudenthome = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // dateTime
            // 
            this.dateTime.Location = new System.Drawing.Point(332, 78);
            this.dateTime.Name = "dateTime";
            this.dateTime.Size = new System.Drawing.Size(150, 21);
            this.dateTime.TabIndex = 28;
            // 
            // txtshiyou
            // 
            this.txtshiyou.Location = new System.Drawing.Point(112, 169);
            this.txtshiyou.Multiline = true;
            this.txtshiyou.Name = "txtshiyou";
            this.txtshiyou.Size = new System.Drawing.Size(462, 48);
            this.txtshiyou.TabIndex = 27;
            // 
            // lblshiyou
            // 
            this.lblshiyou.AutoSize = true;
            this.lblshiyou.Location = new System.Drawing.Point(53, 169);
            this.lblshiyou.Name = "lblshiyou";
            this.lblshiyou.Size = new System.Drawing.Size(53, 12);
            this.lblshiyou.TabIndex = 26;
            this.lblshiyou.Text = "来访事由";
            // 
            // lbltime
            // 
            this.lbltime.AutoSize = true;
            this.lbltime.Location = new System.Drawing.Point(261, 81);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(53, 12);
            this.lbltime.TabIndex = 25;
            this.lbltime.Text = "来访时间";
            // 
            // txtbroomnumber
            // 
            this.txtbroomnumber.Location = new System.Drawing.Point(112, 124);
            this.txtbroomnumber.Name = "txtbroomnumber";
            this.txtbroomnumber.Size = new System.Drawing.Size(110, 21);
            this.txtbroomnumber.TabIndex = 24;
            // 
            // lblbroomnumber
            // 
            this.lblbroomnumber.AutoSize = true;
            this.lblbroomnumber.Location = new System.Drawing.Point(1, 127);
            this.lblbroomnumber.Name = "lblbroomnumber";
            this.lblbroomnumber.Size = new System.Drawing.Size(89, 12);
            this.lblbroomnumber.TabIndex = 23;
            this.lblbroomnumber.Text = "被访学生宿舍号";
            // 
            // txtbname
            // 
            this.txtbname.Location = new System.Drawing.Point(332, 29);
            this.txtbname.Name = "txtbname";
            this.txtbname.Size = new System.Drawing.Size(100, 21);
            this.txtbname.TabIndex = 22;
            // 
            // lblbname
            // 
            this.lblbname.AutoSize = true;
            this.lblbname.Location = new System.Drawing.Point(249, 32);
            this.lblbname.Name = "lblbname";
            this.lblbname.Size = new System.Drawing.Size(77, 12);
            this.lblbname.TabIndex = 21;
            this.lblbname.Text = "被访学生姓名";
            // 
            // txtname
            // 
            this.txtname.Location = new System.Drawing.Point(112, 26);
            this.txtname.Name = "txtname";
            this.txtname.Size = new System.Drawing.Size(100, 21);
            this.txtname.TabIndex = 19;
            // 
            // lblname
            // 
            this.lblname.AutoSize = true;
            this.lblname.Location = new System.Drawing.Point(37, 29);
            this.lblname.Name = "lblname";
            this.lblname.Size = new System.Drawing.Size(53, 12);
            this.lblname.TabIndex = 18;
            this.lblname.Text = "您的姓名";
            // 
            // lblsex
            // 
            this.lblsex.AutoSize = true;
            this.lblsex.Location = new System.Drawing.Point(61, 78);
            this.lblsex.Name = "lblsex";
            this.lblsex.Size = new System.Drawing.Size(29, 12);
            this.lblsex.TabIndex = 17;
            this.lblsex.Text = "性别";
            // 
            // btnnexit
            // 
            this.btnnexit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnnexit.Location = new System.Drawing.Point(276, 247);
            this.btnnexit.Name = "btnnexit";
            this.btnnexit.Size = new System.Drawing.Size(75, 23);
            this.btnnexit.TabIndex = 16;
            this.btnnexit.Text = "取消";
            this.btnnexit.UseVisualStyleBackColor = true;
            this.btnnexit.Click += new System.EventHandler(this.btnnexit_Click);
            // 
            // btndengji
            // 
            this.btndengji.Location = new System.Drawing.Point(154, 247);
            this.btndengji.Name = "btndengji";
            this.btndengji.Size = new System.Drawing.Size(75, 23);
            this.btndengji.TabIndex = 15;
            this.btndengji.Text = "登记";
            this.btndengji.UseVisualStyleBackColor = true;
            this.btndengji.Click += new System.EventHandler(this.btndengji_Click);
            // 
            // lblappintment
            // 
            this.lblappintment.AutoSize = true;
            this.lblappintment.Location = new System.Drawing.Point(261, 130);
            this.lblappintment.Name = "lblappintment";
            this.lblappintment.Size = new System.Drawing.Size(53, 12);
            this.lblappintment.TabIndex = 29;
            this.lblappintment.Text = "在校职位";
            // 
            // txtzhiwu
            // 
            this.txtzhiwu.Location = new System.Drawing.Point(332, 127);
            this.txtzhiwu.Name = "txtzhiwu";
            this.txtzhiwu.Size = new System.Drawing.Size(100, 21);
            this.txtzhiwu.TabIndex = 30;
            // 
            // txtsex
            // 
            this.txtsex.FormattingEnabled = true;
            this.txtsex.Items.AddRange(new object[] {
            "男",
            "女"});
            this.txtsex.Location = new System.Drawing.Point(112, 78);
            this.txtsex.Name = "txtsex";
            this.txtsex.Size = new System.Drawing.Size(121, 20);
            this.txtsex.TabIndex = 31;
            // 
            // btnstudenthome
            // 
            this.btnstudenthome.Location = new System.Drawing.Point(472, 29);
            this.btnstudenthome.Name = "btnstudenthome";
            this.btnstudenthome.Size = new System.Drawing.Size(75, 23);
            this.btnstudenthome.TabIndex = 32;
            this.btnstudenthome.Text = "查看是否存在";
            this.btnstudenthome.UseVisualStyleBackColor = true;
            this.btnstudenthome.Click += new System.EventHandler(this.btnstudenthome_Click);
            // 
            // 校内人员登记
            // 
            this.AcceptButton = this.btndengji;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Menu;
            this.CancelButton = this.btnnexit;
            this.ClientSize = new System.Drawing.Size(623, 310);
            this.Controls.Add(this.btnstudenthome);
            this.Controls.Add(this.txtsex);
            this.Controls.Add(this.txtzhiwu);
            this.Controls.Add(this.lblappintment);
            this.Controls.Add(this.dateTime);
            this.Controls.Add(this.txtshiyou);
            this.Controls.Add(this.lblshiyou);
            this.Controls.Add(this.lbltime);
            this.Controls.Add(this.txtbroomnumber);
            this.Controls.Add(this.lblbroomnumber);
            this.Controls.Add(this.txtbname);
            this.Controls.Add(this.lblbname);
            this.Controls.Add(this.txtname);
            this.Controls.Add(this.lblname);
            this.Controls.Add(this.lblsex);
            this.Controls.Add(this.btnnexit);
            this.Controls.Add(this.btndengji);
            this.ForeColor = System.Drawing.SystemColors.Desktop;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "校内人员登记";
            this.Text = "校内人员登记";
            this.Load += new System.EventHandler(this.校内人员登记_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateTime;
        private System.Windows.Forms.TextBox txtshiyou;
        private System.Windows.Forms.Label lblshiyou;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.TextBox txtbroomnumber;
        private System.Windows.Forms.Label lblbroomnumber;
        private System.Windows.Forms.TextBox txtbname;
        private System.Windows.Forms.Label lblbname;
        private System.Windows.Forms.TextBox txtname;
        private System.Windows.Forms.Label lblname;
        private System.Windows.Forms.Label lblsex;
        private System.Windows.Forms.Button btnnexit;
        private System.Windows.Forms.Button btndengji;
        private System.Windows.Forms.Label lblappintment;
        private System.Windows.Forms.TextBox txtzhiwu;
        private System.Windows.Forms.ComboBox txtsex;
        private System.Windows.Forms.Button btnstudenthome;
    }
}